﻿using System;

namespace PrintLib
{
    public class Class1
    {
        public static void Square() // квадратик
        {
            ConsoleColor BorderColor = ConsoleColor.Cyan; // установка цвета
            Console.ForegroundColor = BorderColor;
            char asterisk = '*';
          
            for (int h = 0; h < 5; h++)
            {
                for (int w = 0; w < 10; w++)
                {
                 Console.Write(asterisk);
                }
              Console.WriteLine();
            }
            Console.WriteLine();
        }
        public static void Elipse() // круглишок
        {
            double radius = 5;
            double thickness = 0.4;
            char symbol = '*';

            Console.WriteLine();
            double rIn = radius - thickness, rOut = radius + thickness;

            for (double y = radius; y >= -radius; --y)
            {
                for (double x = -radius; x < rOut; x += 0.5)
                {
                    double value = x * x + y * y;
                    if (value >= rIn * rIn && value <= rOut * rOut)
                    {
                        Console.Write(symbol);
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine();
        }
        public static void Pyramid() // пирамидка
        {
            int size = 10;
            if (size % 2 == 0) size++;
            for (int i = 0; i < size; i += 2)
            {
               
                for (int j = 0; j < (size - 1) / 2 - (i / 2); j++) 
                    Console.Write(' ');
               
                for (int j = 0; j < i + 1; j++)  
                    Console.Write('*');
                    Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine();
        }
        public static void Romb() // ромб
        {
            int i, j, N = 11;
            int center = N / 2;
            for (i = 0; i < N; i++)
            {
                for (j = 0; j < N; j++)
                {
                    if (i <= center)
                    {
                        if (j >= center - i && j <= center + i)
                            Console.Write("*");
                        else
                            Console.Write(" ");
                    }
                    else
                    {
                        if (j >= center + i - N + 1 && j <= center - i + N - 1)
                            Console.Write("*");
                        else
                            Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine();
        }
        public static void Tree() // дерево
        {

            Console.WriteLine("____________*___________");
            Console.WriteLine("___________*о*__________");
            Console.WriteLine("__________*o*o*_________");
            Console.WriteLine("_________*o*o*o*________");
            Console.WriteLine("________*o*o*o*o* ______");
            Console.WriteLine("______ *o*o*o*o*o* _____");
            Console.WriteLine("__________|| ||_________");
        }
    }

}

